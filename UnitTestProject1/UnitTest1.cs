﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Remote;

namespace CalculatorAutomationWinappDriver
{
    [TestClass]
    public class UnitTest1
    {
        //Appium Driver URL it works like a windows Service on your PC  
        private const string appiumDriverURI = "http://127.0.0.1:4723";        

        private const string calApp = @"C:\Users\Dell\Desktop\Temp\debug\yugamiru.exe";

        protected static WindowsDriver<WindowsElement> calSession;


        [TestMethod]
        public void TestMethod1()
        {
            if (calSession == null)
            {
                //Process.Start(@"C:\Users\Dell\Desktop\Temp\Release\yugamiru.exe");
                //Thread.Sleep(6000);
                DesiredCapabilities appCapabilities = new DesiredCapabilities();
                appCapabilities.SetCapability("app", calApp);
                appCapabilities.SetCapability("deviceName", "WindowsPC");                
                calSession = new WindowsDriver<WindowsElement>(new Uri(appiumDriverURI), appCapabilities);
                                             
                
                calSession.FindElementByAccessibilityId("IDC_MeasurementBtn").Click();
                calSession.FindElementByAccessibilityId("IDC_Name").Click();
                calSession.FindElementByAccessibilityId("IDC_Name").SendKeys("SRIVASTAVA");
                calSession.FindElementByAccessibilityId("IDC_ID").Click();
                calSession.FindElementByAccessibilityId("IDC_ID").SendKeys("SUMITID123");
                calSession.FindElementByAccessibilityId("IDC_COMBO_GENDER").Click();
                calSession.FindElementByAccessibilityId("IDC_COMBO_GENDER").SendKeys(Keys.Down);

                calSession.FindElementByAccessibilityId("IDC_dtp_DOB").SendKeys(Keys.Down);
                calSession.FindElementByAccessibilityId("IDC_dtp_DOB").SendKeys(Keys.Right);
                calSession.FindElementByAccessibilityId("IDC_dtp_DOB").SendKeys(Keys.Up);
                calSession.FindElementByAccessibilityId("IDC_dtp_DOB").SendKeys(Keys.Up);
                calSession.FindElementByAccessibilityId("IDC_dtp_DOB").SendKeys(Keys.Up);


                calSession.FindElementByAccessibilityId("IDC_dtp_DOB").SendKeys(Keys.Right);
                calSession.FindElementByAccessibilityId("IDC_dtp_DOB").SendKeys(Keys.Up);
                calSession.FindElementByAccessibilityId("IDC_dtp_DOB").SendKeys(Keys.Up);
                calSession.FindElementByAccessibilityId("IDC_dtp_DOB").SendKeys(Keys.Up);               

                calSession.FindElementByAccessibilityId("IDC_dtp_DOB").SendKeys(Keys.Enter);

                calSession.FindElementByAccessibilityId("IDC_dtp_DOB").SendKeys(Keys.Down);
                calSession.FindElementByAccessibilityId("IDC_dtp_DOB").SendKeys(Keys.Enter);

                calSession.FindElementByAccessibilityId("txtHeight").Click();
                calSession.FindElementByAccessibilityId("txtHeight").Clear();
                calSession.FindElementByAccessibilityId("txtHeight").SendKeys("532");

                Thread.Sleep(6000);
                calSession.FindElementByAccessibilityId("IDC_NextBtn").Click();

                #region hintComments
                //calSession.FindElementByAccessibilityId("IDC_SearchBtn").Click();


                //calSession.FindElementByAccessibilityId("IDC_ID").SetImmediateValue("SUMITID123");
                //calSession.FindElementByAccessibilityId("IDC_Name").Click();
                //calSession.FindElementByAccessibilityId("IDC_Name").SetImmediateValue("SRIVASTAVA");
                //calSession.FindElementByAccessibilityId("IDC_NextBtn").Click();
                //calSession.FindElement(By.Name("Start")).Click();IDC_NextBtn
                //calSession.FindElement(By.Name("One")).Click();
                //calSession.FindElement(By.Name("Two")).Click();
                //calSession.FindElement(By.Name("Three")).Click();
                //calSession.FindElement(By.Name("Multiply by")).Click();
                ////find by automation id  
                //calSession.FindElementByAccessibilityId("num9Button").Click();
                //calSession.FindElementByAccessibilityId("equalButton").Click();
                ////getting value from textbox  
                //string ExpectedValue = calSession.FindElementByAccessibilityId("CalculatorResults").Text;
                //string ExpectedValue1 = ExpectedValue.Replace("Display is ", "").Replace(",", "");

                ////Testcases  
                //Assert.AreEqual(82107, Convert.ToInt64(ExpectedValue1));
#endregion
            }
        }

        [TestMethod]
        public void TestMethod2()
        {
            //if (calSession == null)
            {
                #region CoveredInMethod1
                //Process.Start(@"C:\Users\Dell\Desktop\Temp\Release\yugamiru.exe");
                //Thread.Sleep(6000);
                //DesiredCapabilities appCapabilities = new DesiredCapabilities();
                //appCapabilities.SetCapability("app", calApp);
                //appCapabilities.SetCapability("deviceName", "WindowsPC");
                ////Create a session to intract with Calculator windows application  
                //calSession = new WindowsDriver<WindowsElement>(new Uri(appiumDriverURI), appCapabilities);


                //Automate Button and Get answer from Calculator  

                //find by Name  



                //calSession.SwitchTo().Window("IMAGE_FILE_SELECT");
#endregion
                //First Image Selection
                calSession.FindElementByAccessibilityId("IDC_SearchBtn").Click();
                var currentWindow1 = calSession.CurrentWindowHandle;
                var availableWindows1 = new List<string>(calSession.WindowHandles);

                foreach (string w in availableWindows1)
                {
                    if (w != currentWindow1)
                    {
                        calSession.SwitchTo().Window(w);
                    }
                }
                calSession.FindElement(By.Name("OK")).Click();
                calSession.SwitchTo().Window(currentWindow1);
                calSession.FindElementByAccessibilityId("IDC_ShootBtn").Click();
                calSession.FindElementByAccessibilityId("IDC_NextBtn").Click();
//First Image Selection END

//Second Image Selection
                calSession.FindElementByAccessibilityId("IDC_SearchBtn").Click();
                var currentWindow2 = calSession.CurrentWindowHandle;
                var availableWindows2 = new List<string>(calSession.WindowHandles);

                foreach (string w in availableWindows2)
                {
                    if (w != currentWindow2)
                    {
                        calSession.SwitchTo().Window(w);
                    }
                }
                calSession.FindElement(By.Name("OK")).Click();
                calSession.SwitchTo().Window(currentWindow2);
                calSession.FindElementByAccessibilityId("IDC_ShootBtn").Click();
                calSession.FindElementByAccessibilityId("IDC_NextBtn").Click();
//Second Image Selection END

//Third Image Selection
                calSession.FindElementByAccessibilityId("IDC_SearchBtn").Click();
                var currentWindow3 = calSession.CurrentWindowHandle;
                var availableWindows3 = new List<string>(calSession.WindowHandles);

                foreach (string w in availableWindows3)
                {
                    if (w != currentWindow3)
                    {
                        calSession.SwitchTo().Window(w);
                    }
                }
                calSession.FindElement(By.Name("OK")).Click();
                calSession.SwitchTo().Window(currentWindow3);
                calSession.FindElementByAccessibilityId("IDC_ShootBtn").Click();
                calSession.FindElementByAccessibilityId("IDC_NextBtn").Click();
                //Third Image Selection END
                calSession.FindElementByAccessibilityId("IDC_OkBtn").Click();
 //Click 'OK' on messagebox 1  START
                var currentWindowM1 = calSession.CurrentWindowHandle;
                var availableWindowsM1 = new List<string>(calSession.WindowHandles);

                foreach (string w in availableWindowsM1)
                {
                    if (w != currentWindowM1)
                    {
                        calSession.SwitchTo().Window(w);
                    }
                }
                calSession.FindElement(By.Name("Yes")).Click();
                calSession.FindElementByAccessibilityId("IDC_OkBtn").Click();
 //Click 'OK' on messagebox 1  END



 //Click 'OK' on messagebox 2 START
                var currentWindowM2 = calSession.CurrentWindowHandle;
                var availableWindowsM2 = new List<string>(calSession.WindowHandles);

                foreach (string w in availableWindowsM2)
                {
                    if (w != currentWindowM2)
                    {
                        calSession.SwitchTo().Window(w);
                    }
                }
                calSession.FindElement(By.Name("Yes")).Click();
                calSession.FindElementByAccessibilityId("IDC_OkBtn").Click();
 //Click 'OK' on messagebox 2 END

 //Click 'OK' on messagebox 3 START
                var currentWindowM3 = calSession.CurrentWindowHandle;
                var availableWindowsM3 = new List<string>(calSession.WindowHandles);

                foreach (string w in availableWindowsM3)
                {
                    if (w != currentWindowM3)
                    {
                        calSession.SwitchTo().Window(w);
                    }
                }
                calSession.FindElement(By.Name("Yes")).Click();
                calSession.FindElementByAccessibilityId("IDC_OkBtn").Click();
                //Click 'OK' on messagebox 3 END


//Click 'OK' on messagebox 4 START
                var currentWindowM4 = calSession.CurrentWindowHandle;
                var availableWindowsM4 = new List<string>(calSession.WindowHandles);

                foreach (string w in availableWindowsM4)
                {
                    if (w != currentWindowM4)
                    {
                        calSession.SwitchTo().Window(w);
                    }
                }
                calSession.FindElement(By.Name("Yes")).Click();
                //calSession.FindElementByAccessibilityId("IDC_OkBtn").Click();
//Click 'OK' on messagebox 4 END

                var currentWindowM45 = calSession.CurrentWindowHandle;
                var availableWindowsM45 = new List<string>(calSession.WindowHandles);

                foreach (string w in availableWindowsM45)
                {
                    if (w != currentWindowM45)
                    {
                        calSession.SwitchTo().Window(w);
                        //break;
                    }
                }
                //calSession.SwitchTo().Window("ResultView");
                //Thread.Sleep(100);
                calSession.FindElementByAccessibilityId("IDC_BTN_DATASAVE").Click();
//Saved Messagebox START
                var currentWindowMSaved = calSession.CurrentWindowHandle;
                var availableWindowsMSaved = new List<string>(calSession.WindowHandles);

                foreach (string w in availableWindowsMSaved)
                {
                    if (w != currentWindowMSaved)
                    {
                        calSession.SwitchTo().Window(w);
                        //break;
                    }
                }
                Thread.Sleep(5000);
                calSession.FindElement(By.Name("OK")).Click();
                calSession.FindElementByAccessibilityId("IDC_ScoresheetBtn").Click();
                //Saved dialog 'OK' click  END

                //Trial Purchase dialog 'No' click  START
                Thread.Sleep(6000);
                var currentWindowMPurchase = calSession.CurrentWindowHandle;
                var availableWindowsMPurchase = new List<string>(calSession.WindowHandles);

                foreach (string w in availableWindowsMPurchase)
                {
                    if (w != currentWindowMPurchase)
                    {
                        calSession.SwitchTo().Window(w);
                        //break;
                    }
                }
                //calSession.Close();
                //calSession.FindElement(By.Name("Close")).Click();
                //calSession.FindElement(By.Name("No")).Click();
                //Trial Purchase dialog 'No' click  END

                #region hintComments
                //IDC_NextBtn   IDC_OkBtn  IDC_BTN_DATASAVE
                //calSession.FindElementByAccessibilityId("IDC_Name").Click();
                //calSession.FindElementByAccessibilityId("IDC_Name").SendKeys("SRIVASTAVA");
                //calSession.FindElementByAccessibilityId("IDC_ID").Click();
                //calSession.FindElementByAccessibilityId("IDC_ID").SendKeys("SUMITID123");
                //calSession.FindElementByAccessibilityId("IDC_COMBO_GENDER").Click();
                //calSession.FindElementByAccessibilityId("IDC_COMBO_GENDER").SendKeys(Keys.Down);

                //Thread.Sleep(6000);
                //calSession.FindElementByAccessibilityId("IDC_NextBtn").Click();



                //calSession.FindElementByAccessibilityId("IDC_ID").SetImmediateValue("SUMITID123");
                //calSession.FindElementByAccessibilityId("IDC_Name").Click();
                //calSession.FindElementByAccessibilityId("IDC_Name").SetImmediateValue("SRIVASTAVA");
                //calSession.FindElementByAccessibilityId("IDC_NextBtn").Click();
                //calSession.FindElement(By.Name("Start")).Click();IDC_NextBtn
                //calSession.FindElement(By.Name("One")).Click();
                //calSession.FindElement(By.Name("Two")).Click();
                //calSession.FindElement(By.Name("Three")).Click();
                //calSession.FindElement(By.Name("Multiply by")).Click();
                ////find by automation id  
                //calSession.FindElementByAccessibilityId("num9Button").Click();
                //calSession.FindElementByAccessibilityId("equalButton").Click();
                ////getting value from textbox  
                //string ExpectedValue = calSession.FindElementByAccessibilityId("CalculatorResults").Text;
                //string ExpectedValue1 = ExpectedValue.Replace("Display is ", "").Replace(",", "");

                ////Testcases  
                //Assert.AreEqual(82107, Convert.ToInt64(ExpectedValue1));
                
#endregion
            }
        }
    }
}